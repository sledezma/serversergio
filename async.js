var fs=require('fs').promises;

function sumar(a,b) {
    return a+b;
}

async function main() {
    var n1 = sumar(1, 2);
    var n2 = sumar(n1, sumar(2,2));
    var n3 = sumar(n1, n2);
    
    try {
        const contenido1 = await fs.readFile('bla.txt');
        console.log("uno", contenido1)
        const contenido2 = await fs.readFile('ble.txt');
        console.log("dos", contenido2)
        const contenido3 = await fs.readFile('bli.txt');
        console.log("tres", contenido3)
    } catch ( e ) {
        console.log(e);
    }    
    //3, 7, 10
    console.log(n1, n2, n3);
}

main()