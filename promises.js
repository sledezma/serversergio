var fs=require('fs').promises;

function sumar(a,b) {
    return a+b;
}

var n1 = sumar(1, 2);
var n2 = sumar(n1, sumar(2,2));
var n3 = sumar(n1, n2);

fs.readFile('bla.txt')
.then( (contenido) => {
    console.log("uno", contenido)
    return fs.readFile('ble.txt');
})
.then( (contenido) => {
    console.log("dos", contenido)
    return fs.readFile('bli.txt');
})
.then( (contenido) => {
    console.log("tres", contenido);
})
.catch( e =>{
    console.log(e);
})




//3, 7, 10
console.log(n1, n2, n3);